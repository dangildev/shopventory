# shopventory

This project uses React and Parcel. I developed using Node version 14.18.3. So you might want to nvm it.

## install dependencies

```
yarn install
```

## run dev server

```
yarn develop
```

Project will run on port 1234.

## run linter

```
yarn lint
```
