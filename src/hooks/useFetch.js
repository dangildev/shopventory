import { useState, useEffect } from 'react';

export const useFetch = (url) => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    let cancelled = false;
    setLoading(true);

    const fetchData = async () => {
      try {
        const response = await fetch(url);
        const body = await response.json();

        setError(null);

        if (!cancelled) {
          setData(body);
        }
      } catch (err) {
        setError(err);
        console.error(err);
      }
    };

    fetchData();
    setLoading(false);

    return () => {
      cancelled = true;
    };
  }, [url]);

  return { data, loading, error };
};
