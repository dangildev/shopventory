import { useContext, useEffect } from 'react';
import { Routes, Route } from 'react-router-dom';
import { GlobalContext } from './context';

import Home from './components/pages/Home/Home';

const websiteConfig = {
  brand: 'Dog App',
  links: [
    { title: 'Home', to: '/' },
    { title: 'About', to: '/about' },
    { title: 'Contact', to: '/contact' },
  ],
  heroTitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  heroDescription:
    'Fusce dapibus lectus turpis, id ultrices ligula hendrerit eu. Curabitur tincidunt nec sapien in mollis. Proin tristique neque sem, eget vehicula dui dignissim eget.',
  heroVideoURL: 'https://www.youtube.com/embed/iXCESZ1Wn7Q',
};

const App = () => {
  const { setGlobalState } = useContext(GlobalContext);

  useEffect(() => {
    setGlobalState((prevState) => ({ ...prevState, ...websiteConfig }));
  }, []);

  return (
    <Routes>
      <Route path="/" element={<Home />} />
    </Routes>
  );
};

export default App;
