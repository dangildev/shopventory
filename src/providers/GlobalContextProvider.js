import PropTypes from 'prop-types';

import { memo, useMemo, useState } from 'react';
import { GlobalContext } from '../context/GlobalContext';

const GlobalContextProvider = ({ children }) => {
  const [globalState, setGlobalState] = useState({});

  const store = useMemo(
    () => ({
      globalState,
      setGlobalState,
    }),
    [globalState]
  );

  return <GlobalContext.Provider value={store}>{children}</GlobalContext.Provider>;
};

GlobalContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default memo(GlobalContextProvider);
