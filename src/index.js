import ReactDOM from 'react-dom';

import { BrowserRouter } from 'react-router-dom';

import App from './App';
import GlobalContextProvider from './providers/GlobalContextProvider';

import './assets/index.scss';

ReactDOM.render(
  <BrowserRouter>
    <GlobalContextProvider>
      <App />
    </GlobalContextProvider>
  </BrowserRouter>,
  document.getElementById('app')
);
