import PropTypes from 'prop-types';

import { useContext } from 'react';
import { Navbar, Hero } from 'UI';
import { GlobalContext } from '../../../context';

import './Main.scss';

export const Main = ({ children }) => {
  const { globalState } = useContext(GlobalContext);
  const {
    brand = '',
    links = [],
    heroTitle = '',
    heroDescription = '',
    heroVideoURL = '',
  } = globalState || {};

  return (
    <>
      <div className="top">
        <Navbar brand={brand} links={links} />
        <Hero title={heroTitle} description={heroDescription} videoURL={heroVideoURL} />
      </div>
      <main className="content">{children}</main>
    </>
  );
};

Main.defaultProps = {
  children: null,
};

Main.propTypes = {
  children: PropTypes.node,
};
