import PropTypes from 'prop-types';

import { createElement } from 'react';

const tags = {
  default: 'p',
  title: 'h1',
  m: 'h2',
  l: 'h3',
  xl: 'h4',
};

export const Typography = ({ size, children, ...props }) =>
  createElement(tags[size], props, children);

Typography.defaultProps = {
  size: 'default',
};

Typography.propTypes = {
  size: PropTypes.oneOf(['default', 'title', 'm', 'l', 'xl']),
  children: PropTypes.node.isRequired,
};
