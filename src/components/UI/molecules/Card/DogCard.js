import PropTypes from 'prop-types';

import { useFetch } from '../../../../hooks';
import { Card } from './Card';

export const DogCard = ({ title, main }) => {
  const path = (main ? `${main}/` : '') + title;
  const { data } = useFetch(`https://dog.ceo/api/breed/${path}/images`);
  const { message: photoURLs = [] } = data || {};

  return <Card title={title} photoURL={photoURLs[0]} />;
};

DogCard.defaultProps = {
  title: '',
  main: null,
};

DogCard.propTypes = {
  title: PropTypes.string,
  main: PropTypes.string,
};
