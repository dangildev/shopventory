import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';

import { Typography } from 'UI';

import './Card.scss';

export const Card = ({ photoURL, title }) => (
  <div className="card">
    <LazyLoad once>
      <img className="card__image" alt={title} src={photoURL} />
    </LazyLoad>
    <Typography className="card__title">{title}</Typography>
  </div>
);

Card.defaultProps = {
  title: '',
  photoURL:
    'data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mN89vz5fwAJJAO1pRhnzAAAAABJRU5ErkJggg==',
};

Card.propTypes = {
  title: PropTypes.string,
  photoURL: PropTypes.string,
};
