import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Typography } from '../../atoms/Typography/Typography';

import './Navbar.scss';

export const Navbar = ({ brand, links }) => (
  <nav className="nav">
    <Typography className="nav__brand" size="m">
      {brand}
    </Typography>
    <ul className="nav__links">
      {links.map((link) => (
        <li className="nav__links--link" key={link.title}>
          <Link to={link.to}>{link.title}</Link>
        </li>
      ))}
    </ul>
  </nav>
);

Navbar.defaultProps = {
  brand: '',
  links: [],
};

Navbar.propTypes = {
  brand: PropTypes.string,
  links: PropTypes.arrayOf(PropTypes.shape({ title: PropTypes.string, to: PropTypes.string })),
};
