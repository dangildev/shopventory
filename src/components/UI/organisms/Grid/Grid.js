import PropTypes from 'prop-types';

import './Grid.scss';

export const Grid = ({ children }) => <div className="grid">{children}</div>;

Grid.defaultProps = {
  children: null,
};

Grid.propTypes = {
  children: PropTypes.node,
};
