import PropTypes from 'prop-types';

import { Typography } from 'UI';

import './Hero.scss';

export const Hero = ({ title, description, videoURL }) => (
  <header className="hero">
    <div className="hero__left">
      <Typography size="title">{title}</Typography>
      <Typography>{description}</Typography>
    </div>
    <div className="hero__right">
      <iframe
        width="560"
        height="315"
        src={videoURL}
        title="YouTube video player"
        frameBorder="0"
      />
    </div>
  </header>
);

Hero.defaultProps = {
  title: '',
  description: '',
  videoURL: 'https://www.youtube.com/embed/iXCESZ1Wn7Q',
};

Hero.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  videoURL: PropTypes.string,
};
