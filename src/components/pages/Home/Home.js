import { DogCard, Grid, Typography } from 'UI';
import { Main } from '../../templates';
import { useFetch } from '../../../hooks';

const Home = () => {
  const { data } = useFetch('https://dog.ceo/api/breeds/list/all') || {};

  const { message: breedsObject = {} } = data || {};

  const breedsArray = Object.keys(breedsObject);

  return (
    <Main>
      <Typography size="m" style={{ textAlign: 'center' }}>
        Dog Breeds
      </Typography>
      <Grid>
        {breedsArray.map((key) => {
          const subBreeds = breedsObject[key];

          if (subBreeds.length > 0) {
            return subBreeds.map((breed) => <DogCard key={breed} title={breed} main={key} />);
          }

          return <DogCard key={key} title={key} />;
        })}
      </Grid>
    </Main>
  );
};

export default Home;
